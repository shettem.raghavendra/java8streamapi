package com.antra.raghu;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * GIVEN A LIST OF INTEGERS, 
 * SORT ALL THE VALUES PRESENT IN IT IN DESCENDING ORDER
 * USING STREAM FUNCTIONS
 * @author Shettem Raghavendra
 *
 */
public class SortCollectionsInDesc {

	public static void main(String args[]) {
        List<Integer> myList = Arrays.asList(10,15,8,49,25,98,98,32,15);

         myList.stream()
               .sorted(Collections.reverseOrder())
               .forEach(System.out::println);
}
}
