package com.antra.raghu;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/***
 * Find Sum of Collection values using Stream API
 * @author Shettem Raghavendra
 *
 */
class Employee {
	String name;
	Double Sal;

	public Employee(String name, Double sal) {
		super();
		this.name = name;
		Sal = sal;
	}

	public String getName() {
		return name;
	}

	public Double getSal() {
		return Sal;
	}
	

}

public class SumOfListValues {

	public static void main(String[] args) {
		// format 1
		List<Integer> list = Arrays.asList(10, 12, 33, 45, 67);
		int sum = list.stream().mapToInt(i -> i.intValue()).sum();
		System.out.println(sum);

		// format 2 (best)
		int result = list.stream().reduce(0, Integer::sum);
		System.out.println(result);

		//format 3
		int data = list.stream().collect(Collectors.summingInt(Integer::intValue));
		System.out.println(data);
		
		List<Employee> empList = Arrays.asList(
				new Employee("AJAY",500.20),
				new Employee("SAM",600.20),
				new Employee("SURAJ",700.20)
				);
		double totalSal = empList.stream().mapToDouble(i->i.getSal()).sum();
		System.out.println(totalSal);
	}
}
