package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

/**
 * GIVEN A LIST OF INTEGERS, FIND OUT ALL THE NUMBERS STARTING WITH 1 USING
 * STREAM FUNCTIONS
 * 
 * @author Shettem Raghavendra
 *
 */
public class FindValuesStartsWith {

	public static void main(String args[]) {
		List<Integer> myList = Arrays.asList(10, 15, 8, 49, 25, 98, 32);
		
		myList.stream().map(s -> s.toString()) // Convert integer to String
				.filter(s -> s.startsWith("1"))
				.forEach(System.out::println);
	}
}
