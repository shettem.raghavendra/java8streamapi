package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

/**
 * GIVEN A LIST OF INTEGERS, 
 * FIND THE TOTAL NUMBER OF ELEMENTS PRESENT IN THE LIST
 * USING STREAM FUNCTIONS
 * 
 * @author Shettem Raghavendra
 *
 */
public class SizeOfCollections {
	public static void main(String args[]) {
		List<Integer> myList = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
		long count = myList.stream().count();
		System.out.println(count);
	}
}
