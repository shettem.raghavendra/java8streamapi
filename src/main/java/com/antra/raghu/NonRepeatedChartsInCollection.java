package com.antra.raghu;

import java.util.LinkedHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/***
 * GIVEN A STRING, FIND THE FIRST NON-REPEATED CHARACTER IN IT 
 * USING STREAM FUNCTIONS
 * @author Shettem Raghavendra
 *
 */
public class NonRepeatedChartsInCollection {
	public static void main(String args[]) {
        String input = "Hello Antraiens From Raghu";

        Character result = input.chars() // Stream of String       
                                .mapToObj(s -> Character.toLowerCase(Character.valueOf((char) s))) // First convert to Character object and then to lowercase         
                                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting())) //Store the chars in map with count 
                                .entrySet()
                                .stream()
                                .filter(entry -> entry.getValue() == 1L)
                                .map(entry -> entry.getKey())
                                .findFirst()
                                .get();
        System.out.println(result);                    
}
}
