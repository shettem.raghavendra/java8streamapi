package com.antra.raghu;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * HOW TO FIND DUPLICATE ELEMENTS 
 * IN A GIVEN INTEGERS LIST 
 * IN JAVA USING STREAM FUNCTIONS
 * 
 * @author Shettem Raghavendra
 *
 */
public class FindDuplicatesInList {

	public static void main(String args[]) {
		List<Integer> myList = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
		
		Set<Integer> set = new HashSet<>();
		
		myList.stream()
		.filter(n -> !set.add(n)) //false if element is already added
		.forEach(System.out::println);
	}
}
