package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

/**
 * GIVEN A LIST OF INTEGERS, 
 * FIND THE MAXIMUM VALUE ELEMENT PRESENT IN IT 
 * USING STREAM FUNCTIONS
 * 
 * @author Shettem Raghavendra
 *
 */
public class FindMaxFormList {

	public static void main(String args[]) {
        List<Integer> myList = Arrays.asList(10,15,8,49,25,98,98,32,15);
        int max =  myList.stream()
                         .max(Integer::compare)
                         .get();
        System.out.println(max);                    
}
}
