package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Shettem Raghavendra
 * 
 * Given a list of integers, 
 * Find out all the even numbers exist in the list 
 * using Stream functions
 *
 */
public class FindEvenValuesFormCollection {

	public static void main(String args[]) {
		List<Integer> myList = Arrays.asList(10,15,8,49,25,98,32);
		myList.stream()
		.filter(n -> n%2 == 0)
		.forEach(System.out::println);
	}
}
