package com.antra.raghu;

import java.util.Arrays;
import java.util.List;

/**
 * GIVEN THE LIST OF INTEGERS, FIND THE FIRST ELEMENT OF THE LIST USING STREAM
 * FUNCTIONS
 * 
 * @author Shettem Raghavendra
 *
 */
public class FindFirstFromCollections {

	public static void main(String args[]) {
		List<Integer> myList = Arrays.asList(10, 15, 8, 49, 25, 98, 98, 32, 15);
		myList.stream()
		.findFirst()
		.ifPresent(System.out::println);
	}
}
