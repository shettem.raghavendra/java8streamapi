package com.example.demo;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MostRepeatedCharsinString {
	public static void main(String[] args) {
		char c = findCharsCount("Hello from Java");
		System.out.println(c);
	}
	public static char findCharsCount(String str) {
		Map<Character,Integer> map = new LinkedHashMap<>();         
		char[] carr = str.toCharArray();
		for(Character ch: carr) {
			if(map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
		}
		Integer maxVal = Collections.max(map.values());
		char tempChar = Character.MIN_VALUE;
		for (Entry<Character, Integer> entry:map.entrySet()) {
			if (entry.getValue() == maxVal) {
				tempChar = entry.getKey(); 
				break;
			}
		}
		return tempChar;
	}
}










